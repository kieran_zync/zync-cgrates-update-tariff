# README #

### Update CGRates Tariff Plans ###

## Steps ##

1. Run 'createCgratesCsvFiles.py' with required pricing spreadsheets located in the ‘/pricing’ directory, current required currencies are AUD, EUR, GBP, USD

2. Upload created CSV files to the CGRATES server run 'gcloud compute copy-files *.csv be-billing-asia1:/home/[some directory] --zone asia-east1-a' note: make sure your cloud is pointing to correct project i.e. zync-cpm-01 or zync-voice-test

3. To UPDATE CGRates run  'cgr-loader -verbose=true -validate=true -dry_run=false -flushdb=false -tpid="default"  -path=/home/[some directory]'

4. Or to REPLACE all CGRates tariff plans run 'cgr-loader -verbose=true -validate=true -dry_run=false -flushdb=true -tpid="default"  -path=/home/kiernhackshall/20151113_load'

5. To update the historical CGRates StoreDB with new tariff plans run 'cgr-loader -stordb_host="173.194.107.139" -stordb_name="cgrates" -stordb_port="3306" -stordb_type="mysql" -stordb_passwd="[password]" -stordb_user="cgrates" -to_stordb=true -tpid="default" -validate=true -verbose=true'