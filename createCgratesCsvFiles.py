import xlrd
import csv
import json

print 'Reading excel...'
print ''

countries = ['AUD', 'EUR','GBP','USD']

ratingPlans = ['basic','standard','advanced','premium','enterprise']

# create headers for the csv files

with open('billing/RatingProfiles.csv', 'w') as csvfile:
    
    fieldnames = ['#Direction','Tenant','Category','Subject','ActivationTime','RatingPlanId','RatesFallbackSubject','CdrStatQueueIds']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()


with open('billing/RatingPlans.csv', 'w') as csvfile:
    
    fieldnames = ['#Tag','DestinationRatesTag','TimingTag','Weight']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
       
    
with open('billing/DestinationRates.csv', 'w') as csvfile:
        
    fieldnames = ['#Tag','DestinationsTag','RatesTag','RoundingMethod','RoundingDecimals','MaxCost','MaxCostStrategy']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
 
        
with open('billing/Destinations.csv', 'w') as csvfile:
        
    fieldnames = ['#Tag','Prefix']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    
    
with open('billing/Rates.csv', 'w') as csvfile:
        
    fieldnames = ['#Tag','ConnectFee','Rate','RateUnit','RateIncrement','GroupIntervalStart']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    


    
webDict = {}
webDictTmp = {}

outboundWritten = False

def readRows(curr):
    
    count = 1 
    
    country = ''
    type = ''
    basic = ''
    standard = ''
    advanced = ''
    premium = ''
    enterprise = ''
    description = ''
    
    fixedDone = False
    
    createdDestnation = False
    
    global outboundWritten
    outboundWritten = False
    
    
    
    
    
    for rownum in range(1,sh1.nrows):
            print 'debug: %s'% rownum
            rows = sh1.row_values(rownum)
            
            plans = {}
            
            if rows[0] != '':
                country = rows[0]
            
            print 'country: %s'% rows[0]    
            print 'countryCode: %s'% rows[1]
            print 'type: %s'% rows[4]
            countryCode = rows[1].lower()
            currency = rows[2].lower()
            mode = rows[3]    
            type = rows[4]
            description = rows[5]
            base = rows[6]
            basic = rows[7]
            
            plans = {"base": { "baseFixedRate": rows[6], "baseFixedUnit": rows[7],"baseFixedIncrement": rows[8],"baseFixedStart": rows[9],"baseRate": rows[10],"baseUnit": rows[11],"baseIncrement": rows[12],"baseStart": rows[13]} }
            
            '''
            if type == 'mobile':
                plans.update({"basic": { "basicFixedRate": rows[14], "basicFixedUnit": '0',"basicFixedIncrement": '0',"basicFixedStart": rows[17],"basicRate": rows[18],"basicUnit": rows[19],"basicIncrement": rows[20],"basicStart": '0'} })
            else:
            '''
                
            plans.update({"basic": { "basicFixedRate": rows[14], "basicFixedUnit": rows[15],"basicFixedIncrement": rows[16],"basicFixedStart": rows[17],"basicRate": rows[18],"basicUnit": rows[19],"basicIncrement": rows[20],"basicStart": rows[21]} })
                
            plans.update({"standard": { "standardFixedRate": rows[22], "standardFixedUnit": rows[23],"standardFixedIncrement": rows[24],"standardFixedStart": rows[25],"standardRate": rows[26],"standardUnit": rows[27],"standardIncrement": rows[28],"standardStart": rows[29]} })
            plans.update({"advanced": { "advancedFixedRate": rows[30], "advancedFixedUnit": rows[31],"advancedFixedIncrement": rows[32],"advancedFixedStart": rows[33],"advancedRate": rows[34],"advancedUnit": rows[35],"advancedIncrement": rows[36],"advancedStart": rows[37]} })
            plans.update({"premium": { "premiumFixedRate": rows[38], "premiumFixedUnit": rows[39],"premiumFixedIncrement": rows[40],"premiumFixedStart": rows[41],"premiumRate": rows[42],"premiumUnit": rows[43],"premiumIncrement": rows[44],"premiumStart": rows[45]} })
            plans.update({"enterprise": { "enterpriseFixedRate": rows[46], "enterpriseFixedUnit": rows[47],"enterpriseFixedIncrement": rows[48],"enterpriseFixedStart": rows[49],"enterpriseRate": rows[50],"enterpriseUnit": rows[51],"enterpriseIncrement": rows[52],"enterpriseStart": rows[53]} })
            
            
            
            
            destinations = str(rows[54])
            
            if count > 9:
                    count = 1
                    fixedDone = True
                    
                    outboundWritten = False
                    
                    if type != '' and countryCode != 'cy' and countryCode != 'CY' and countryCode != 'tc' and countryCode != 'TC':
                        
                        createFixed(countryCode)
            

            if type != '' and countryCode != 'cy' and countryCode != 'CY' and countryCode != 'tc' and countryCode != 'TC':
                
                if plans['base']['baseFixedRate'] != 42:
                
                    create = createJsonFile(country, countryCode, currency, mode, type, plans)
                    
                    # set the fixed charges
                    if type == 'mobilenumberrental' or type == 'mobilenumbersetup' or type == 'landlinenumberrental' or type == 'email' or type == 'webimpression' or type == 'datacapture':
                        
                        created = createRatingProfiles(countryCode, currency, mode, type)
                            
                        created = createRatingPlans(countryCode, currency, mode, type, plans, description)
                        
                        created = createDestinationRates(countryCode, currency, mode, type, plans)
                    
                        created = createRates(countryCode, currency, mode, type, plans)
                        
                    else:
                    
                        createdDestination, fixedDone = createDestinations(countryCode, type, destinations, fixedDone)
                        
                        if createdDestination == True:
                            
                            created = createRatingProfiles(countryCode, currency, mode, type)
                            
                            created = createRatingPlans(countryCode, currency, mode, type, plans, description)
                            
                            created = createDestinationRates(countryCode, currency, mode, type, plans)
                        
                            created = createRates(countryCode, currency, mode, type, plans)
                            
                            createdDestination = False
                

            
            count += 1
        
    
     
        
    jsonWebDict = json.dumps(webDict)
    
    fileName = 'website/Pricing_%s.json' % curr
    
    f = open(fileName,'w')
    f.write(jsonWebDict) 
    f.close()

    #print jsonWebDict

def createJsonFile(country,countryCode,currency,mode,type,plans):
    
    global webDict
    
    
    webDictTmp ={}
    
    created = False
    
    if type == 'minimum':
        
        print 'Found minimum'
        

        print int(plans['basic']['basicRate'])
        print int(plans['standard']['standardRate'])
        print int(plans['advanced']['advancedRate'])
        print int(plans['premium']['premiumRate'])
        print int(plans['enterprise']['enterpriseRate'])
        
        webDict['minimum'] = {}
        webDict['minimum']['basic'] = str(int(plans['basic']['basicRate']))
        webDict['minimum']['standard'] = str(int(plans['standard']['standardRate']))
        webDict['minimum']['advanced'] = str(int(plans['advanced']['advancedRate']))
        webDict['minimum']['premium'] = str(int(plans['premium']['premiumRate']))
        webDict['minimum']['enterprise'] = str(int(plans['enterprise']['enterpriseRate']))

        
        
        
    else:
    
    
        if country not in webDict:
            webDict[country] = {}
            webDict[country]['countryCode'] = countryCode
            webDict[country]['currency'] = currency
        
        if type not in webDict[country]:
            webDict[country][type] = {}
        
        for rec in ratingPlans:
            
            if rec == 'basic' or rec == 'standard' or rec == 'advanced' or rec == 'premium' or rec == 'enterprise':
                
                webDict[country][type][rec] = {}
                
                
                fixedRate = '%sFixedRate' % rec
                rate = '%sRate' % rec
                
                    
                fixedStart = '%sFixedStart' % rec
                fixedUnit = '%sFixedUnit' % rec
                fixedIncrement = '%sFixedIncrement' % rec
                
                
                if str(plans[rec][fixedRate]) == "42":
                    webDict[country][type][rec]['fixedRate'] = ''
                else:
                    webDict[country][type][rec]['fixedRate'] = str(plans[rec][fixedRate])
                    
                    
                if str(plans[rec][fixedUnit]) == '42':
                    webDict[country][type][rec]['fixedUnit'] = ''
                else:
                    webDict[country][type][rec]['fixedUnit'] = str(plans[rec][fixedUnit])
                
                
                if str(plans[rec][fixedIncrement]) == '42':
                    webDict[country][type][rec]['fixedIncrement'] = ''
                else:
                    webDict[country][type][rec]['fixedIncrement'] = str(plans[rec][fixedIncrement])
                    
                
                if str(plans[rec][fixedStart]) == '42':
                    webDict[country][type][rec]['fixedStart'] = ''
                else:
                    webDict[country][type][rec]['fixedStart'] = str(plans[rec][fixedStart])           
        
                start = '%sStart' % rec
                unit = '%sUnit' % rec
                increment = '%sIncrement' % rec
                
                
                if str(plans[rec][rate]) == '42':
                    webDict[country][type][rec]['rate'] = ''
                else:
                    webDict[country][type][rec]['rate'] = str(plans[rec][rate])
                
                
                if str(plans[rec][unit]) == '42':
                    webDict[country][type][rec]['unit'] = ''
                else:
                    webDict[country][type][rec]['unit'] = str(plans[rec][unit])
                    
                    
                if str(plans[rec][increment]) == '42':
                    webDict[country][type][rec]['increment'] = ''
                else:
                    webDict[country][type][rec]['increment'] = str(plans[rec][increment])
                
                
                if str(plans[rec][start]) == '42':    
                    webDict[country][type][rec]['start'] = ''
                else:
                    webDict[country][type][rec]['start'] = str(plans[rec][start])
     
    
    
    return created
    
            
def createRatingProfiles(countryCode, currency, mode, type):
    
    createSuccess = False
    
    global outboundWritten
    
    direction = '*out'
    tenant = 'zync.com.au'
    category = ''
    subject = ''
    activationTime = '2015-10-11T13:00:20Z'
    ratingPlanId = ''
    ratesFallbackSubject = ''
    cdrStatQueueIds = ''

    
    with open('billing/RatingProfiles.csv', 'a') as csvfile:
            
        fieldnames = ['#Direction','Tenant','Category','Subject','ActivationTime','RatingPlanId','RatesFallbackSubject','CdrStatQueueIds']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    
        for rate in ratingPlans:
                            
            if type == 'mobile' or type == 'landline':
                
                #print type
                #print outboundWritten
                
                #if outboundWritten == False:
                    
                category = '%s.%s.%s.%s.%s' % (rate,mode,'outbound',currency,countryCode)
                ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,'outbound',currency,countryCode)
                subject = 'outbound.voice'
                
                writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime, 'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
            
            elif type == 'inbound':

                category = '%s.%s.%s.%s.%s' % (rate,mode,'inbound',currency,countryCode)
                ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,'inbound',currency,countryCode)
                subject = 'inbound.voice'
            
                writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
            
            elif type == 'email':
                
                category = '%s.%s.%s.%s.%s' % (rate,'email','outbound',currency,'none')
                ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,'email','outbound',currency,'none')
                subject = 'Outbound.email'
            
                writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
            
            
            elif type == 'outboundmt':
                
                category = '%s.%s.%s.%s.%s' % (rate,mode,'outbound',currency,countryCode)
                ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,'outbound',currency,countryCode)
                subject = 'Outbound.sms'
            
                writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
            
            elif type == 'inboundmo':
                
                #print type
                
                category = '%s.%s.%s.%s.%s' % (rate,mode,'inbound',currency,countryCode)
                ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,'inbound',currency,countryCode)
                subject = 'Inbound.sms'
            
                writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
             
            elif type == 'mobilenumberrental' or type == 'mobilenumbersetup' or type == 'landlinenumberrental':
        
                    category = '%s.%s.%s.%s.%s' % (rate,mode,type,currency,countryCode)
                    ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,type,currency,countryCode)
                    subject = 'fixed.voice'
                
                    writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
                    
            elif type == 'webimpression':
        
                    category = '%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                    ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                    subject = 'outbound.web'
                
                    writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
                    
            elif type == 'datacapture':
        
                    category = '%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                    ratingPlanId = 'rp.%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                    subject = 'outbound.data'
                
                    writer.writerow({'#Direction': direction, 'Tenant': tenant, 'Category': category, 'Subject': subject, 'ActivationTime': activationTime,  'RatingPlanId': ratingPlanId, 'RatesFallbackSubject': ratesFallbackSubject, 'CdrStatQueueIds': cdrStatQueueIds})
        
        
        outboundWritten = True   
        
        
    
    return createSuccess



def createRatingPlans(countryCode, currency, mode, type, plans,description):
    
    createSuccess = False
    
    tag = ''
    destinationRate = ''
    timingTag = 't.always'
    weight = '10.00'
        
        
    with open('billing/RatingPlans.csv', 'a') as csvfile:

            fieldnames = ['#Tag','DestinationRatesTag','TimingTag','Weight']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            
            if plans['base']['baseFixedRate'] != 42:           
            
                for rate in ratingPlans:
                    
                    if description == 'fixed':
                    
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,mode,type,currency,countryCode)
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,mode,type,currency,countryCode)
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                    
                    elif type == 'email':
                        
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,'email','outbound',currency,'none')
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,'email','outbound',currency,'none')
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                        
                    elif type == 'webimpression':
                    
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                        
                    elif type == 'datacapture':
                    
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,mode,type,currency,'none')
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                        
                    elif type == 'outboundmt':
                        
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,mode,'outbound',currency,countryCode)
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,mode,'outbound',currency,countryCode)
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                        
                    elif type == 'inboundmo':
                        
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,mode,'inbound',currency,countryCode)
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,mode,'inbound',currency,countryCode)
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                        
                    else:
                        
                        tag = 'rp.%s.%s.%s.%s.%s' % (rate,mode,description,currency,countryCode)
                        
                        destinationRate = 'dr.%s.%s.%s.%s.%s' % (rate,mode,type,currency,countryCode)
                        
                        writer.writerow({'#Tag': tag,'DestinationRatesTag': destinationRate,'TimingTag': timingTag,'Weight': weight})
                    
      
                
    
    return createSuccess



def createDestinationRates(countryCode, currency, mode, type, plans):
    
    createSuccess = False
    
    tag = ''
    destinationsTag = ''
    ratesTag = ''
    roundingMethod = ''
    roundingDecimals = '0'
    maxCost = '0.0000'
    maxCostStrategy = ''
    
    if plans['base']['baseFixedRate'] != 42: 
        
        with open('billing/DestinationRates.csv', 'a') as csvfile:
            
            fieldnames = ['#Tag','DestinationsTag','RatesTag','RoundingMethod','RoundingDecimals','MaxCost','MaxCostStrategy'] 
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
            for rate in ratingPlans:
                    
                #tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, type, currency, countryCode)
                
                if type == 'mobile' or type == 'landline' or type == 'inbound' or type == 'tollfreeinbound' or type == 'tollfreeoutbound':
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, type, currency, countryCode)
                    
                    destinationsTag = 'dst.%s.%s' % (type, countryCode)
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, mode, type, currency, countryCode)
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
                
                elif type == 'email':
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, 'email', 'outbound', currency, 'none')
                    
                    destinationsTag = 'dst.%s.%s' % ('email', 'none')
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, 'email', 'outbound', currency, 'none')
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
                    
                elif type == 'webimpression':
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, type, currency, 'none')
                    
                    destinationsTag = 'dst.%s.%s' % ('web', 'none')
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, mode, type, currency, 'none')
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
                    
                elif type == 'datacapture':
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, type, currency, 'none')
                    
                    destinationsTag = 'dst.%s.%s' % ('data', 'none')
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, mode, type, currency, 'none')
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
                  
                elif type == 'outboundmt':
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, 'outbound', currency, countryCode)
                    
                    destinationsTag = 'dst.%s.%s' % ('smsoutbound', countryCode)
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, mode, 'outbound', currency, countryCode)
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
                
                elif type == 'inboundmo':
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, 'inbound', currency, countryCode)
                    
                    destinationsTag = 'dst.%s.%s' % ('smsinbound', countryCode)
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, mode, 'inbound', currency, countryCode)
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
                   
                else:
                    
                    tag = 'dr.%s.%s.%s.%s.%s' % (rate, mode, type, currency, countryCode)
                    
                    destinationsTag = 'dst.%s.%s' % ('fixed', countryCode)
                    
                    ratesTag = 'rt.%s.%s.%s.%s.%s' % (rate, mode, type, currency, countryCode)
                    
                    writer.writerow({'#Tag': tag, 'DestinationsTag': destinationsTag, 'RatesTag': ratesTag, 'RoundingMethod': roundingMethod, 'RoundingDecimals': roundingDecimals, 'MaxCost': maxCost, 'MaxCostStrategy': maxCostStrategy})
        
    
    
    return createSuccess


def getNumbers(countryCode,type):
    
    '''
    if type == 'mobile' or type == 'outboundmt' or type == 'inboundmo':
        book1 = xlrd.open_workbook('numbers/MobileNumbers.xls') 
        sh1 = book1.sheet_by_index(0)
    else:
        book1 = xlrd.open_workbook('numbers/LandlineNumbers.xls') 
        sh1 = book1.sheet_by_index(0)
    '''
    
    numbersString = ''
    
    firstRec = True
    
    #print countryCode
    
    if type == 'landline' or type == 'tollfreeinbound' or type == 'tollfreeoutbound' or type == 'inbound':
        
        book1 = xlrd.open_workbook('numbers/LandlineNumbers.xls') 
        sh1 = book1.sheet_by_index(0)
    
        if type == 'landline':
            
            for rownum in range(1,sh1.nrows):
                
                    rows = sh1.row_values(rownum)
                    
                    if rows[1].lower() == countryCode:
                        if rows[6] != '':
                            if rows[29] != 'yes':
                                if firstRec == True:
                                    numbersString = '%s' % int(rows[6])
                                    firstRec = False
                                else:
                                    numbersString += ',%s' % int(rows[6])
                                #print '%s: Landline: %s' % (rows[0],int(rows[6]))
                                
        if type == 'tollfreeinbound' or type == 'tollfreeoutbound':
            
            for rownum in range(1,sh1.nrows):
                
                    rows = sh1.row_values(rownum)
                    
                    if rows[1].lower() == countryCode:
                        if rows[6] != '':
                            if rows[29] == 'yes':
                                if firstRec == True:
                                    numbersString = '%s' % int(rows[6])
                                    firstRec = False
                                else:
                                    numbersString += ',%s' % int(rows[6])
                                    
        if type == 'inbound':
            
            for rownum in range(1,sh1.nrows):
                
                    rows = sh1.row_values(rownum)
                    if rows[5] != '':
                        if rows[0].lower() == countryCode:
                        
                            #print '%s %s' % (countryCode, int(rows[5]))
                            
                            numbersString = '%s' % int(rows[5])
                                
                                
    elif type == 'mobile' or type == 'outboundmt' or type == 'inboundmo':
        
        book1 = xlrd.open_workbook('numbers/MobileNumbers_updated.xls') 
        sh1 = book1.sheet_by_index(0)
        
        for rownum in range(1,sh1.nrows):
                
            rows = sh1.row_values(rownum)
            
            if rows[1].lower() == countryCode:
                
                if rows[6] != '':
                    
                    if firstRec == True:
                        numbersString = '%s' % int(rows[6])
                        firstRec = False
                    else:
                        numbersString += ',%s' % int(rows[6])
        

    return numbersString

def createDestinations(countryCode, type, destinations, fixedDone):
    
    createSuccess = False
    
    tag = ''
        
    #print type
    
    numbersString = getNumbers(countryCode,type)
    
    #print '%s: %s' % (countryCode,numbersString)
    
    #print countryCode
    
    with open('billing/Destinations.csv', 'a') as csvfile:
        
        fieldnames = ['#Tag','Prefix']
            
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        if numbersString != '':

            numbers = numbersString.split(',')
            
            if numbers[0] != '':
                
                createSuccess = True
                
                for num in numbers:
                    
                    if type == 'outboundmt':
                    
                        tag = 'dst.%s.%s' % ('smsoutbound', countryCode)
                        
                    elif type == 'inboundmo':
                        
                        tag = 'dst.%s.%s' % ('smsinbound', countryCode)
                        
                    elif type == 'email':
                        
                        tag = 'dst.%s.%s' % ('email', 'none')
                    
                    elif type == 'web':
                        
                        tag = 'dst.%s.%s' % ('web', 'none')
                        
                    elif type == 'data':
                        
                        tag = 'dst.%s.%s' % ('data', 'none')
                        
                    else:
                        
                        tag = 'dst.%s.%s' % (type, countryCode)
                            
                    
                    writer.writerow({'#Tag': tag, 'Prefix': num})
                    
    
    return createSuccess, fixedDone


def createFixed(countryCode):
    
    with open('billing/Destinations.csv', 'a') as csvfile:
        
        fieldnames = ['#Tag','Prefix']
            
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        tag = 'dst.%s.%s' % ('fixed', countryCode)
        
        writer.writerow({'#Tag': tag, 'Prefix': 'fixed'})
        

def createEmailDestination():

    with open('billing/Destinations.csv', 'a') as csvfile:
        
        fieldnames = ['#Tag','Prefix']
            
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        tag = 'dst.%s.%s' % ('email', 'none')
        
        writer.writerow({'#Tag': tag, 'Prefix': 'email-'})
        
        
def createWebDestination():

    with open('billing/Destinations.csv', 'a') as csvfile:
        
        fieldnames = ['#Tag','Prefix']
            
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        tag = 'dst.%s.%s' % ('web', 'none')
        
        writer.writerow({'#Tag': tag, 'Prefix': 'web-'})
        
        
def createDataDestination():

    with open('billing/Destinations.csv', 'a') as csvfile:
        
        fieldnames = ['#Tag','Prefix']
            
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        tag = 'dst.%s.%s' % ('data', 'none')
        
        writer.writerow({'#Tag': tag, 'Prefix': 'data-'})

    
        

def createRates(countryCode, currency, mode, type, plans):
    
    createSuccess = False
    
    tag = ''
    connectFee = '0.0000'
    rate = ''
    rateIncrement = '1'
    
    # need to check where this is getting set from
    rateUnit = '60'
    
    # need to check where this is getting set from
    groupIntervalStart = '0'
    
    if plans['base']['baseFixedRate'] != 42: 

        with open('billing/Rates.csv', 'a') as csvfile:
        
            fieldnames = ['#Tag','ConnectFee','Rate','RateUnit','RateIncrement','GroupIntervalStart']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            
            
            
            
            for rec in ratingPlans:
                
                tag = 'rt.%s.%s.%s.%s.%s' % (rec,mode,type,currency,countryCode)
                
                if rec == 'basic' or rec == 'standard' or rec == 'advanced' or rec == 'premium' or rec == 'enterprise':
                    
                    fixedRate = '%sFixedRate' % rec
                    rate = '%sRate' % rec
                    
                    if plans[rec][fixedRate] != '':
                        
                        if type == 'email':
                            
                            tag = 'rt.%s.%s.%s.%s.%s' % (rec,'email','outbound',currency,'none')
                            
                        if type == 'webimpression':
                            
                            tag = 'rt.%s.%s.%s.%s.%s' % (rec,mode,type,currency,'none')
                            
                        if type == 'datacapture':
                            
                            tag = 'rt.%s.%s.%s.%s.%s' % (rec,mode,type,currency,'none')
                        
                        if type == 'outboundmt':
                            
                            tag = 'rt.%s.%s.%s.%s.%s' % (rec,mode,'outbound',currency,countryCode)
                            
                        if type == 'inboundmo':
                            
                            tag = 'rt.%s.%s.%s.%s.%s' % (rec,mode,'inbound',currency,countryCode)
                        
                        fixedStart = '%sFixedStart' % rec
                        fixedUnit = '%sFixedUnit' % rec
                        fixedIncrement = '%sFixedIncrement' % rec
                        
                        if plans[rec][fixedUnit] != '':
                        
                            writer.writerow({'#Tag': tag, 'ConnectFee': connectFee, 'Rate': plans[rec][fixedRate], 'RateUnit': plans[rec][fixedUnit], 'RateIncrement': plans[rec][fixedIncrement], 'GroupIntervalStart': plans[rec][fixedStart]})
                        
            
                    if plans[rec][rate] != '':
                    
                        start = '%sStart' % rec
                        unit = '%sUnit' % rec
                        increment = '%sIncrement' % rec
                        
                        
                        if plans[rec][unit] != '':
                            
                            writer.writerow({'#Tag': tag, 'ConnectFee': connectFee, 'Rate': plans[rec][rate], 'RateUnit': plans[rec][unit], 'RateIncrement': plans[rec][increment], 'GroupIntervalStart': plans[rec][start]})
                        

    
    return createSuccess

# email has only one destination so create once
createEmailDestination() 

# web has only one destination so create once
createWebDestination() 

# web has only one destination so create once
createDataDestination() 

# iterate through country currencies 'AUD', 'EUR','GBP', 'USD'
for country in countries:
    
    fileName = 'pricing/Pricing_%s.xls' % country
    
    print 'Creating currency %s' % country
    
    book = xlrd.open_workbook(fileName) 
    sh1 = book.sheet_by_index(0)
    
    readRows(country)
